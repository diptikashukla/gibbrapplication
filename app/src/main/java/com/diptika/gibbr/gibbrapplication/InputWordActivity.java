package com.diptika.gibbr.gibbrapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Diptika on 5/11/2016.
 */
public class InputWordActivity extends AppCompatActivity {
    private Button okBtn;
    private EditText enterWord;
    private TextView countText, hintText;
    private String updatedString="";
    int count=0;
    int wordsLeft=0;
    private String str="Gibbr is a hot new <adjective> app that lets you post updates from around you! Forget <plural-noun> groups, and Facebook events! Reach out to a wider network instantaneously! Vote, <noun>, and reply to your favourite feeds from <adjective> you! Your local Gibbr community based on your geo location. Get on Gibbr now and be part of a thriving new <noun> social space!\n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_word);

        okBtn=(Button)findViewById(R.id.ok_btn);
        enterWord=(EditText)findViewById(R.id.edit_word);
        countText=(TextView)findViewById(R.id.text2);
        hintText=(TextView)findViewById(R.id.text3);
       // readFile();

        final Map<String, String>wordsToreplace = getWordsToFill(str);
        final List<String> words = new ArrayList<>(wordsToreplace.keySet());
        wordsLeft = wordsToreplace.size();
        count = wordsLeft;

        countText.setText(count+" words left");
        hintText.setText("Please Enter " + words.get(count-1));
        enterWord.setHint(""+words.get(count-1));

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(wordsLeft>1){
                    updatedString=enterWord.getText().toString();
                    wordsToreplace.put(words.get(count-wordsLeft),updatedString);
                    wordsLeft--;
                    countText.setText(wordsLeft + " words left");
                    hintText.setText("Please Enter " + words.get(wordsLeft-1));
                    enterWord.setHint(""+words.get(wordsLeft-1));
                    enterWord.setText("");

                } else if(wordsLeft==1){
                    updatedString=enterWord.getText().toString();
                    wordsToreplace.put(words.get(count-wordsLeft),updatedString);
                    wordsLeft--;

                    updatedString = replaceWordsFromUserInput(str, wordsToreplace);
                    enterWord.setText("");
                    InputWordActivity.this.finish();
                    Intent finalIntent=new Intent(InputWordActivity.this,FinalTextActivity.class);
                    finalIntent.putExtra("Fill Story",updatedString);
                    startActivity(finalIntent);
                }

        }
        });
    }



    private Map<String, String> getWordsToFill(String fileContent){
        final Map<String, String> replaceWordsMap = new HashMap<>();
        List<String> words = Arrays.asList(fileContent.split(" "));
        for(String word : words){
            if(word.matches("<(.*)>")){
                replaceWordsMap.put(word, "");
            }
        }
        return replaceWordsMap;
    }

    private String replaceWordsFromUserInput(String fileContent, Map<String, String> updateWords){
        for(Map.Entry<String, String> entry : updateWords.entrySet()){
            fileContent =  fileContent.replaceAll(entry.getKey(), entry.getValue());
        }
        return fileContent;

    }

    private String replace(String fileContent, String updatedWord){
        fileContent =  fileContent.replaceAll("<(.*)>",updatedWord);
        return fileContent;
    }

    private void readFile() {
//        String data = "";
//        StringBuffer sbuffer = new StringBuffer();
//        InputStream is = this.getResources().openRawResource(R.drawable.gibbr_fill);
//        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
//        try {
//            while ((data = reader.readLine()) != null)
//            {
//                sbuffer.append(data + "\n");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }


}