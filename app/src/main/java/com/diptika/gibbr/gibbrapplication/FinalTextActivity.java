package com.diptika.gibbr.gibbrapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


/**
 * Created by Diptika on 5/11/2016.
 */
public class FinalTextActivity extends AppCompatActivity {
    private TextView finalText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_text);

        finalText=(TextView)findViewById(R.id.text1);
        Bundle bundle=getIntent().getExtras();
        String story=bundle.getString("Fill Story");
        finalText.setText(story);

    }
}