package com.diptika.gibbr.gibbrapplication;

/**
 * Created by Diptika on 5/11/2016.
 */
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {
    private Button startBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        startBtn=(Button)findViewById(R.id.start_btn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inputWordIntent=new Intent(HomeActivity.this,InputWordActivity.class);
                startActivity(inputWordIntent);
            }
        });
    }
}
